//
// Created by vromanch on 12/19/16.
//

#ifndef SYSTEM_CALLS_REGISTER_H
#define SYSTEM_CALLS_REGISTER_H

#if __WORDSIZE == 64
#define REG(reg) reg.orig_rax
#else
#define REG(reg) reg.orig_eax
#endif

#endif //SYSTEM_CALLS_REGISTER_H
