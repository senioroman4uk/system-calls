#include <sys/ptrace.h>
#include <iostream>
#include <unistd.h>
#include <wait.h>
#include <sys/user.h>
#include <map>
#include "callTypes.h"
#include "register.h"

const std::map<CallTypes, std::string> typeNames = {
        {ProcessControl, "ProcessControl"}, {FileManipulation, "FileManipulation"},
        {DeviceManipulation, "DeviceManipulation"}, {InformationMaintanance, "InformationMaintanance"},
        {Communication, "Communication"}, {Protection, "Protection"}
};



int main(int argc, char* argv[]) {
    if (argc == 1) {
        std::cout << "not enough arguments" << std::endl;
        exit(EXIT_FAILURE);
    }

    char* chargs[argc];
    for (int i = 0; i < argc - 1; i++)
        chargs[i] = argv[i+1];
    chargs[argc - 1] = NULL;

    pid_t child = fork();
    if (child == 0) {
        ptrace(PTRACE_TRACEME, 0, NULL, NULL);
        execvp(chargs[0], chargs);
    }
    else {
        int status;
        std::map<CallTypes, int> results = {{ProcessControl,         0},
                                            {FileManipulation,       0},
                                            {DeviceManipulation,     0},
                                            {InformationMaintanance, 0},
                                            {Communication,          0},
                                            {Protection,             0}
                                           };

        int missing = 0;
        while (waitpid(child, &status, 0) && !WIFEXITED(status)) {
            struct user_regs_struct regs;
            ptrace(PTRACE_SYSCALL, child, NULL, NULL);
            // Todo: perhaps there is a need to implement (or use) some kind of sandbox
            ptrace(PTRACE_GETREGS, child, NULL, &regs);
            auto key = REG(regs);
            if (SystemCalls.count(key))
                results[SystemCalls.at(key)]++;
            else
            {
                missing++;
            }
        }

        std::cout << std::endl << "Results" << std::endl;
        for (auto pair : results)
            std::cout << typeNames.at(pair.first) << " - " << pair.second << std::endl;
        std::cout << "Unknown calls:  " << missing << std::endl;
    }

    return EXIT_SUCCESS;
}
