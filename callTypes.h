//
// Created by vromanch on 12/19/16.
//

#ifndef SYSTEM_CALLS_TYPES_H
#define SYSTEM_CALLS_TYPES_H

#include <sys/syscall.h>

enum CallTypes {
    ProcessControl,
    FileManipulation,
    DeviceManipulation,
    InformationMaintanance,
    Communication,
    Protection
};

static const std::map<long long, CallTypes> SystemCalls = {
        { SYS_open, FileManipulation }, { SYS_close, FileManipulation}, { SYS_read, FileManipulation },
        { SYS_write, FileManipulation}, { SYS_chdir, FileManipulation }, { SYS_mkdir, FileManipulation },
        { SYS_rmdir, FileManipulation}, { SYS_unlink, FileManipulation }, { SYS_lstat, FileManipulation },
        { SYS_lseek, FileManipulation }, { SYS_fstat,FileManipulation },
        { SYS_fgetxattr, FileManipulation }, {SYS_lgetxattr, FileManipulation}, {SYS_getxattr, FileManipulation},

        { SYS_fork, ProcessControl }, { SYS_exit, ProcessControl }, { SYS_wait4, ProcessControl }, { SYS_waitid, ProcessControl }, { SYS_execve, ProcessControl },
        { SYS_ioctl, DeviceManipulation }, { SYS_mount, DeviceManipulation }, { SYS_umount2, DeviceManipulation },
        { SYS_getpid, InformationMaintanance }, { SYS_alarm, InformationMaintanance},
        { SYS_pipe, Communication, }, { SYS_pipe2, Communication  }, { SYS_mmap, Communication }, { SYS_munmap, Communication }, { SYS_shmget, Communication },
        { SYS_stat, Communication, }, { SYS_newfstatat, Communication }, { SYS_rt_sigaction, Communication}, { SYS_rt_sigpending, Communication},
        { SYS_rt_sigprocmask, Communication}, { SYS_arch_prctl,Communication },
        { SYS_brk, Communication },
        { SYS_chmod, Protection, }, { SYS_chown, Protection }, { SYS_umask, Protection }, { SYS_access, Protection },
        { SYS_mprotect, Protection },

};

#endif //SYSTEM_CALLS_TYPES_H
